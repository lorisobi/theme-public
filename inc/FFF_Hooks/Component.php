<?php
/**
 * WP_Rig\WP_Rig\FFF_Hooks\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\FFF_Hooks;

use WP_Rig\WP_Rig\Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_action;
use function add_filter;
use function function_exists;

/**
 * Class for adding custom theme enqueues
 */
class Component implements Component_Interface {

	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'fff_hooks';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
		add_filter( 'body_class', [ $this, 'filter_body_classes_add_marginless' ] );
		add_filter( 'wp_rig_css_files', [ $this, 'filter_css_files' ] );
		add_filter( 'wp_rig_google_fonts', [ $this, 'filter_google_fonts' ] );

		add_filter( 'wp_resource_hints', [ $this, 'filter_resource_hints' ], 10, 2 );

		add_action( 'wp_enqueue_scripts', [ $this, 'action_enqueue_vendor_styles' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'action_enqueue_vendor_scripts' ], 1 );
		add_action( 'wp_head', [$this, 'action_preload_fonts'] );

		// COBLOCKS: maybe remove (google) font settings.
		add_filter( 'coblocks_tablet_breakpoint', [ $this, 'filter_coblocks_tablet_breakpoint' ] );
		add_filter( 'coblocks_desktop_breakpoint', [ $this, 'filter_coblocks_desktop_breakpoint' ] ); // seems to be the mobile-up breakpoint.
		/* add_filter( 'coblocks_typography_controls_enabled', '__return_false' ); */
	}

	/**
	 * Adds a 'is-marginless' class to the array of body classes for layouts that have footer margins removed.
	 *
	 * @param array $classes Classes for the body element.
	 * @return array Filtered body classes.
	 */
	public function filter_body_classes_add_marginless( array $classes ) : array {

		if ( ! is_singular() ) {
			return $classes;
		}

		if ( function_exists( 'get_field' ) && get_field( 'is-marginless' ) ) {
			$classes[] = 'is-marginless';
		}

		return $classes;
	}

	/**
	 * Filters the enqueued stylesheets.
	 *
	 * @param array $css_files holds all to be enqueued css files.
	 *
	 * @return array Associative array of $handle => $data pairs.
	 */
	public function filter_css_files( array $css_files ) : array {

		$file_path = get_theme_file_path( '/assets/css/' ) . 'vendor.min.css';

		if ( file_exists( $file_path ) ) {
			$css_files['vendor'] = [
				'file' => 'vendor.min.css',
				'global' => true,
				'preload_callback' => '__return_true',
			];
		}

		return $css_files;
	}

	/**
	 * Filters the google fonts array.
	 *
	 * @param array $fonts  google fonts array.
	 *
	 * @return array Associative array of $font_name => $font_variants pairs.
	 */
	public function filter_google_fonts( array $fonts ) : array {
		$fonts = [];

		/**
		 * Holds all google fonts
		 *
		 * @param array $fonts Associative array of $font_name => $font_variants pairs
		 */
		return $fonts;
	}

	/**
	 * Adds preconnect resource hint for Jost Font.
	 *
	 * @param array  $urls          URLs to print for resource hints.
	 * @param string $relation_type The relation type the URLs are printed.
	 * @return array URLs to print for resource hints.
	 */
	public function filter_resource_hints( array $urls, string $relation_type ) : array {
		if ( 'preconnect' === $relation_type && wp_style_is( 'jost-font', 'queue' ) ) {
			$urls[] = [
				'href' => 'https://indestructibletype.com',
				'crossorigin',
			];
		}

		return $urls;
	}

	/**
	 * Registers or enqueues vendor stylesheets.
	 *
	 * Add custom styles & fonts
	 */
	public function action_enqueue_vendor_styles() {
		wp_enqueue_style( 'jost-font', 'https://indestructibletype.com/fonts/Jost.css', [], '3.4', 'all' );
	}

	/**
	 * Preloads local font files
	 *
	 */
	public function action_preload_fonts() {

        $fonts_path = get_theme_file_uri('assets/fonts/');
        
        // Cardenio Modern
        $cardenio_modern = array(
            'CardenioModern-Bold',
            'CardenioModern-Regular'
        );
       
        // Merriweather Sans
        $merriweather_sans = array(
            'merriweather-sans-v11-latin-300',
            'merriweather-sans-v11-latin-300italic',
            'merriweather-sans-v11-latin-regular',
            'merriweather-sans-v11-latin-italic',
            'merriweather-sans-v11-latin-700',
            'merriweather-sans-v11-latin-700italic'
        );
       
        // WOFF2
        foreach(array_merge($cardenio_modern, $merriweather_sans) as $font) {
            echo '<link rel="preload" href="' . $fonts_path . $font . '.woff2" as="font" type="font/woff2" crossorigin>';
        }

    }

	/**
	 * Registers or enqueues vendor scripts.
	 *
	 * Add custom scripts
	 */
	public function action_enqueue_vendor_scripts() {

		$file_path = get_theme_file_path( '/assets/js/' ) . 'vendor.min.js';

		if ( file_exists( $file_path ) ) {
			wp_enqueue_script( 'headroom', get_stylesheet_directory_uri() . '/assets/js/vendor.min.js', [], wp_rig()->get_asset_version( $file_path ), true );
		}
	}

	/**
	 * Adjusts the coblocks tablet breakpoint to be the same as in theme. Doesn't work for row/columns block though.
	 * see assets/css/src/_custom-media.css for reference
	 *
	 * @param string $breakpoint Tablet breakpoint value.
	 * @return string Filtered tablet breakpoint.
	 */
	public function filter_coblocks_tablet_breakpoint( string $breakpoint ) : string {
		return '1024px';
	}

	/**
	 * Adjusts the coblocks desktop  breakpoint to be the same as in theme. Doesn't work for row/columns block though.
	 * see assets/css/src/_custom-media.css for reference
	 *
	 * @param string $breakpoint desktop breakpoint value.
	 * @return string Filtered desktop breakpoint.
	 */
	public function filter_coblocks_desktop_breakpoint( string $breakpoint ) : string {
		return '768px';
	}

}
