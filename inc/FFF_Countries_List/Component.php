<?php
/**
 * WP_Rig\WP_Rig\FFF_Countries_List\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\FFF_Countries_List;

use WP_Rig\WP_Rig\Component_Interface;
use WP_Rig\WP_Rig\Templating_Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_action;
use function add_filter;
use function add_shortcode;
use function get_fields;

/**
 * Class for adding country related logic
 *
 * Exposes template tags:
 * * `wp_rig()->get_countries()`
 */
class Component implements Component_Interface, Templating_Component_Interface {

	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'FFF_Countries_List';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
		add_filter( 'wp_rig_css_files', [ $this, 'filter_css_files' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'action_enqueue_feed_scripts' ] );
		add_shortcode( 'fff_country_list', [ $this, 'add_country_list_shortcode' ] );
	}

	/**
	 * Filters the enqueued stylesheets.
	 * conditionally add additional country-list styles
	 *
	 * @param array $css_files holds all to be enqueued css files.
	 *
	 * @return array Associative array of $handle => $data pairs.
	 */
	public function filter_css_files( array $css_files ) : array {

		$file_path = get_theme_file_path( '/assets/css/' ) . 'country-list.min.css';

		if ( file_exists( $file_path ) ) {
			$css_files['wp-rig-country-list'] = [
				'file' => 'country-list.min.css',
				'preload_callback' => function() {
					global $post;
					return is_singular() && has_shortcode( $post->post_content, 'fff_country_list' );
				},
			];
		}

		return $css_files;
	}

/**
	 * Registers and enqueues required scripts.
	 */
	public function action_enqueue_feed_scripts() {

		$file_path = get_theme_file_path( '/assets/js/' ) . 'country-list.min.js';

		if ( file_exists( $file_path ) ) {
			wp_enqueue_script( 'country-list', get_stylesheet_directory_uri() . '/assets/js/country-list.min.js', [], wp_rig()->get_asset_version( $file_path ), true );
			wp_localize_script(
				'country-list',
				'jsOpts',
				[
					'restURL' => rest_url(),
					'restNonce' => wp_create_nonce( 'country-list-rest' ),
				]
			);
		}
	}

	/**
	 * Gets template tags to expose as methods on the Template_Tags class instance, accessible through `wp_rig()`.
	 *
	 * @return array Associative array of $method_name => $callback_info pairs. Each $callback_info must either be
	 *               a callable or an array with key 'callable'. This approach is used to reserve the possibility of
	 *               adding support for further arguments in the future.
	 */
	public function template_tags() : array {
		return array(
			'get_countries' => array( $this, 'get_countries_with_meta' ),
		);
	}

	/**
	 * Gets all countries for the listing.
	 *
	 * @return array list of countries.
	 */
	public function get_countries_with_meta() : array {

		$countries = get_posts(
			array(
				'post_type' => 'country',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby' => 'post_title',
				'order' => 'ASC',
			)
		);

		// extend the post data with all existing acf fields.
		if ( ! empty( $countries ) ) {
			foreach ( $countries as $country ) :
				$country->fields = get_fields( $country->ID );
			endforeach;
		}

		/* error_log( 'countries w/ fields: ' . var_export( $countries, 1 ), 0 ); */

		return $countries;

	}

	/**
	 * Add country list shortcode.
	 * gets and displays the relevant template part.
	 *
	 * @param string $atts shortcode attributes (none yet. should be of type array then).
	 *
	 * @return string template content.
	 */
	public static function add_country_list_shortcode( string $atts ) : string {
		/*
		// uncomment, in case custom attributes are needed.
		$atts = shortcode_atts(
			[
				'foo' => 'no foo',
			],
			$atts,
			'fff_country_list'
		);
		*/

		ob_start();

		get_template_part( 'template-parts/shortcodes/country-list', 'content' );

		return ob_get_clean();
	}


}
