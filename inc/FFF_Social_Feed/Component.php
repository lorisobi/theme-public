<?php
/**
 * WP_Rig\WP_Rig\FFF_Social_Feed\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\FFF_Social_Feed;

use WP_Rig\WP_Rig\Component_Interface;
use WP_Rig\WP_Rig\Templating_Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_action;
use function add_filter;
use function add_shortcode;
use function get_fields;

/**
 * Class for adding acf blocks
 *
 * Exposes template tags:
 * * `wp_rig()->get_social_feed_items()`
 */
class Component implements Component_Interface, Templating_Component_Interface {

	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'fff_social_feed';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
		add_filter( 'wp_rig_css_files', [ $this, 'filter_css_files' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'action_enqueue_feed_scripts' ] );
		add_shortcode( 'fff_social_feed', [ $this, 'add_social_feed_shortcode' ] );
		add_action( 'rest_api_init', [ $this, 'register_rest_routes' ] );
	}

	/**
	 * Filters the enqueued stylesheets.
	 * conditionally add additional social-feed styles
	 *
	 * @param array $css_files holds all to be enqueued css files.
	 *
	 * @return array Associative array of $handle => $data pairs.
	 */
	public function filter_css_files( array $css_files ) : array {
		$file_path = get_theme_file_path( '/assets/css/' ) . 'social-feed.min.css';

		if ( file_exists( $file_path ) ) {
			$css_files['wp-rig-social-feed'] = [
				'file' => 'social-feed.min.css',
				'preload_callback' => function () {
						global $template, $post;
						return ( 'front-page.php' === basename( $template ) || ( is_singular() && has_shortcode( $post->post_content, 'fff_social_feed' ) ) );
				},
			];
		}

		return $css_files;
	}

	/**
	 * Registers and enqueues required scripts.
	 */
	public function action_enqueue_feed_scripts() {

		$file_path = get_theme_file_path( '/assets/js/' ) . 'social-media.min.js';

		if ( file_exists( $file_path ) ) {
			wp_enqueue_script( 'social-media', get_stylesheet_directory_uri() . '/assets/js/social-media.min.js', [], wp_rig()->get_asset_version( $file_path ), true );
			wp_localize_script(
				'social-media',
				'jsOpts',
				[
					'restURL' => rest_url(),
					'restNonce' => wp_create_nonce( 'social-media-rest' ),
				]
			);
		}
	}

	/**
	 * Gets template tags to expose as methods on the Template_Tags class instance, accessible through `wp_rig()`.
	 *
	 * @return array Associative array of $method_name => $callback_info pairs. Each $callback_info must either be
	 *               a callable or an array with key 'callable'. This approach is used to reserve the possibility of
	 *               adding support for further arguments in the future.
	 */
	public function template_tags() : array {
		return array(
			'get_social_feed_items' => array( $this, 'get_feed_items' ),
		);
	}

	/**
	 * Gets all social feed items for the listing.
	 *
	 * @param int $offset current query offset.
	 *
	 * @return array list of social feed items.
	 */
	public function get_feed_items( int $offset = null ) : array {

		if ( empty( $offset ) ) {
			$offset = 0;
		}

		$items = get_posts(
			[
				'post_type' => 'social_media_posts',
				'post_status' => 'publish',
				'posts_per_page' => 12,
				'offset' => $offset,
			]
		);

		// extend the post data with all existing acf fields.
		if ( ! empty( $items ) ) {
			foreach ( $items as $item ) :
				$item->fields = get_fields( $item->ID );
			endforeach;
		}

		/* error_log( 'feed items w/ fields: ' . var_export( $items, 1 ), 0 ); */

		return $items;
	}

	/**
	 * Add social-feed shortcode.
	 * gets and displays the relevant template part.
	 *
	 * @param string $atts shortcode attributes (none yet. should be of type array then).
	 *
	 * @return string template content.
	 */
	public static function add_social_feed_shortcode( string $atts ) : string {
		ob_start();

		get_template_part( 'template-parts/shortcodes/social-feed', 'content' );

		return ob_get_clean();
	}


	/**
	 * Adds a REST API Route to be used for load-more functionality.
	 */
	public function register_rest_routes() {

		register_rest_route(
			'load/more',
			'/smp/(?P<offset>\d+)',
			[
				'methods' => 'GET',
				'callback' => [ $this, 'load_more_feed_items' ],
			]
		);

	}

	/**
	 * Loads more feed items.
	 *
	 * @param \WP_REST_Request $request data from the API request.
	 */
	public function load_more_feed_items( \WP_REST_Request $request ) {

		$params = $request->get_params();

		if ( isset( $params['offset'] ) && is_numeric( $params['offset'] ) ) {
			$offset = $params['offset'];
		} else {
			$offset = 0;
		}

		$items = $this->get_feed_items( $offset );

		if ( empty( $items ) ) {
			wp_send_json_error( 'Error on fetching feed items' );
		}

		ob_start();

		set_query_var( 'feed_items', $items );
		get_template_part( 'template-parts/shortcodes/social-feed', 'items' );

		wp_send_json_success( ob_get_clean() );
	}
}
