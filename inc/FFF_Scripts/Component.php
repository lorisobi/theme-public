<?php
/**
 * WP_Rig\WP_Rig\FFF_scripts\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\FFF_Scripts;

use WP_Rig\WP_Rig\Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_action;
use function add_filter;
use function get_fields;

/**
 * Class for adding acf blocks
 *
 * Exposes template tags:
 * * `wp_rig()->get_scripts_items()`
 */
class Component implements Component_Interface {

    /**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'fff_scripts';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
        add_action('acf/init', [ $this,'acf_scripts_init']);
        add_action('wp_enqueue_scripts', [ $this, 'init_cookie_controller']);

        add_filter( 'gform_confirmation_anchor', '__return_true' );

	}
    
    public function acf_scripts_init() {
    
        // Check function exists.
        if( function_exists('acf_add_options_page') ) {
    
            // Register options page.
            $option_page = acf_add_options_page(array(
                'page_title'    => __('Embedded Scripts'),
                'menu_title'    => __('Embedded Scripts'),
                'icon_url'      => 'dashicons-editor-code',
                'menu_slug'     => 'embedded-scripts-settings',
                'position'      => '199',
                'capability'    => 'edit_themes',
                'redirect'      => false
            ));
        }
    }
    // Adds the options page fields to header.
    public function init_cookie_controller() {
 
        the_field('cookie_controller_script', 'option'); 
        the_field('analytics_script', 'option');
        the_field('other_scripts', 'option');

    }
}