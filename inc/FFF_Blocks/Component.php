<?php
/**
 * WP_Rig\WP_Rig\FFF_Blocks\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\FFF_Blocks;

use WP_Rig\WP_Rig\Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_action;
use function add_filter;

/**
 * Class for adding custom (acf) blocks
 */
class Component implements Component_Interface {

	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'fff_blocks';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
		add_action( 'acf/init', [ $this, 'acf_add_gutenberg_blocks' ] );
	}


	/**
	 * Adds custom gutenberg blocks powered by ACF.
	 */
	public function acf_add_gutenberg_blocks() {

		if ( ! function_exists( 'acf_register_block' ) ) {
			return;
		}

		// Add a carousel/slider block.
		/* acf_register_block(
			[
				'name' => 'acf-carousel',
				'title' => __( 'Carousel (ACF)', 'wp-rig' ),
				'description' => __( 'A custom carousel block powered by ACF.', 'wp-rig' ),
				'render_template' => 'template-parts/blocks/acf-carousel.php',
				'category' => 'media',
				'icon' => 'slides',
				'keywords' => [ 'slider', 'carousel' ],
			]
		); */

	}

}
