<?php
/**
 * WP_Rig\WP_Rig\Editor\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\Editor;

use WP_Rig\WP_Rig\Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_action;
use function add_theme_support;

/**
 * Class for integrating with the block editor.
 *
 * @link https://wordpress.org/gutenberg/handbook/extensibility/theme-support/
 */
class Component implements Component_Interface {

	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'editor';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
		add_action( 'after_setup_theme', [ $this, 'action_add_editor_support' ] );
		add_action( 'enqueue_block_editor_assets', [ $this, 'action_add_editor_scripts' ] );
		add_filter( 'render_block', [ $this, 'filter_editor_block_output' ], 10, 2 );

		// uncomment if loading of bundled coblocks icons block should be disabled.
		/* add_filter( 'coblocks_bundled_icons_enabled', '__return_false' ); */
	}


	/**
	 * Adds support for various editor features.
	 */
	public function action_add_editor_support() {
		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Add support for default block styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for wide-aligned images.
		add_theme_support( 'align-wide' );

		/**
		 * Add support for color palettes.
		 *
		 * To preserve color behavior across themes, use these naming conventions:
		 * - Use primary and secondary color for main variations.
		 * - Use `theme-[color-name]` naming standard for standard colors (red, blue, etc).
		 * - Use `custom-[color-name]` for non-standard colors.
		 *
		 * Add the line below to disable the custom color picker in the editor.
		 * add_theme_support( 'disable-custom-colors' );
		 */
		add_theme_support(
			'editor-color-palette',
			[
				[
					'name'  => __( 'Primary', 'wp-rig' ),
					'slug'  => 'theme-primary',
					'color' => '#1b7340',
				],
				[
					'name'  => __( 'Secondary', 'wp-rig' ),
					'slug'  => 'theme-secondary',
					'color' => '#90d3ed',
				],
				[
					'name'  => __( 'Red', 'wp-rig' ),
					'slug'  => 'theme-red',
					'color' => '#C0392B',
				],
				[
					'name'  => __( 'Green', 'wp-rig' ),
					'slug'  => 'theme-green',
					'color' => '#1da64a',
				],
				[
					'name'  => __( 'Blue', 'wp-rig' ),
					'slug'  => 'theme-blue',
					'color' => '#00a0d2',
				],
				[
					'name'  => __( 'Yellow', 'wp-rig' ),
					'slug'  => 'theme-yellow',
					'color' => '#F1C40F',
				],
				[
					'name'  => __( 'Black', 'wp-rig' ),
					'slug'  => 'theme-black',
					'color' => '#000000',
				],
				[
					'name'  => __( 'Grey', 'wp-rig' ),
					'slug'  => 'theme-grey',
					'color' => '#95A5A6',
				],
				[
					'name'  => __( 'White', 'wp-rig' ),
					'slug'  => 'theme-white',
					'color' => '#FFFFFF',
				],
				[
					'name'  => __( 'Smoke', 'wp-rig' ),
					'slug'  => 'theme-smoke',
					'color' => '#ECF0F1',
				],
				[
					'name'  => __( 'Dusty daylight', 'wp-rig' ),
					'slug'  => 'custom-daylight',
					'color' => '#97c0b7',
				],
				[
					'name'  => __( 'Dusty sun', 'wp-rig' ),
					'slug'  => 'custom-sun',
					'color' => '#eee9d1',
				],
			]
		);

		/*
		 * Add support custom font sizes.
		 *
		 * Add the line below to disable the custom color picker in the editor.
		 * add_theme_support( 'disable-custom-font-sizes' );
		 */
		add_theme_support(
			'editor-font-sizes',
			[
				[
					'name'      => __( 'Small', 'wp-rig' ),
					'shortName' => __( 'S', 'wp-rig' ),
					'size'      => 16,
					'slug'      => 'small',
				],
				[
					'name'      => __( 'Medium', 'wp-rig' ),
					'shortName' => __( 'M', 'wp-rig' ),
					'size'      => 22,
					'slug'      => 'medium',
				],
				[
					'name'      => __( 'Medium Large', 'wp-rig' ),
					'shortName' => __( 'ML', 'wp-rig' ),
					'size'      => 30,
					'slug'      => 'medium-large',
				],
				[
					'name'      => __( 'Large', 'wp-rig' ),
					'shortName' => __( 'L', 'wp-rig' ),
					'size'      => 39,
					'slug'      => 'large',
				],
				[
					'name'      => __( 'Larger', 'wp-rig' ),
					'shortName' => __( 'XL', 'wp-rig' ),
					'size'      => 52,
					'slug'      => 'larger',
				],
			]
		);
	}

	/**
	 * Adds the block additions script and styles
	 */
	public function action_add_editor_scripts() {

		// add jost font.
		wp_enqueue_style( 'jost-font', 'https://indestructibletype.com/fonts/Jost.css', [], '3.4', 'all' );

		// add custom block additions.
		$version = wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/' ) . 'blocks.min.js' );
		wp_enqueue_script( 'wp-rig-editor-additions', get_theme_file_uri( '/assets/js/' ) . 'blocks.min.js', [ 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' ], $version, true );
	}

	/**
	 * Apply some basic block render modifications.
	 *
	 * @param string $content block HTML content.
	 * @param array  $block full block data.
	 *
	 * @return string filtered HTML content
	 */
	public function filter_editor_block_output( string $content, array $block ) : string {
		switch ( $block['blockName'] ) :

			// add inner span to be able to underline only the heading content.
			case 'core/heading':
				if ( strpos( $content, '<span>' ) === false ) {
					$content = str_replace(
						[ '">', '</' ],
						[ '"><span>', '</span></' ],
						$content
					);
				}
				break;

		endswitch;

		return $content;
	}
}
