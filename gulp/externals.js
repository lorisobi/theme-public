/* eslint-env es6 */
'use strict';

// External dependencies
/**
 * External dependencies
 */
import { src, dest, series } from 'gulp';
import pump from 'pump';

/**
 * Internal dependencies
 */
import { paths, gulpPlugins } from './constants';


// 2DO: we might elaborate a SCSS-compiling-solution here
export function externalStyles(done) {

	return pump( [
		src( paths.styles.externals ),
		gulpPlugins.newer( paths.styles.dest ),
		gulpPlugins.concat('vendor.min.css'),
		dest( paths.styles.dest ),
		gulpPlugins.touchFd(),   // set timestamp to 'now'
	], done );

}


export function externalScripts(done) {

	return pump( [
		src( paths.scripts.externals ),
		gulpPlugins.newer( paths.scripts.dest ),
		/*gulpPlugins.rename( {
			suffix: '.min',
		} ),*/
		gulpPlugins.concat('vendor.min.js'),
		dest( paths.scripts.dest ),
		gulpPlugins.touchFd(),   // set timestamp to 'now'
	], done );

}


/**
 * Copy external script and style dependencies into theme assets folder
 * @param {function} done function to call when async processes finish
 */
export default function externals( done ) {

	externalStyles();
	externalScripts()

	done();

}
