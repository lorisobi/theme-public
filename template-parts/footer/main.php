<?php
/**
 * Template part for displaying the footer content
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

?>

<div class="footer-main">

	<?php
	if ( has_nav_menu( 'primary' ) ) :
		?>
		<div class="footer-col">
		<?php
		wp_nav_menu(
			[
				'container_class' => 'footer-nav',
				'theme_location'  => 'primary',
			]
		);
		?>
		</div>
	<?php endif; ?>

	<?php
	if ( wp_rig()->is_footer_sidebar_active() ) :
		wp_rig()->print_styles( 'wp-rig-widgets' );
		?>
		<div class="footer-col">
			<aside id="footer-widgets" class="footer-sidebar widget-area">
			<?php wp_rig()->display_footer_sidebar(); ?>
			</aside><!-- #footer-widgets -->
		</div>
	<?php endif; ?>

</div><!-- .footer-main -->
