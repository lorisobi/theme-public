<?php
/**
 * Template part for displaying the footer info
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

?>

<div class="site-info">
	<span class="site-info__span">
		<?php echo esc_html( strtoupper( get_bloginfo( 'name' ) ) ); ?> <?php echo esc_html( date( 'Y' ) ); ?>
	</span>
	<span class="site-info__span">
		<?php
		if ( function_exists( 'the_privacy_policy_link' ) ) :
			the_privacy_policy_link( '<span class="sep"> | </span>' );
		endif;
		?>
		<?php  // We may decide to remove this later, but lets keep it for now 
		$terms_page = get_page_by_title( 'About the Website' );
		if ( is_a( $terms_page, 'WP_Post' ) && 'publish' === $terms_page->post_status ) :
			?>
			& <a href="<?php echo esc_url( get_permalink( $terms_page->ID ) ); ?>"><?php echo esc_html( $terms_page->post_title ); ?></a>
		<?php endif;  ?>
	
	</span>

</div><!-- .site-info -->
