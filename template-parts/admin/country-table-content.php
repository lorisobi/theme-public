<?php
/**
 * The template for displaying the FFF country table
 *
 * Uses CPT data
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

// Load custom styles.
wp_rig()->print_styles( 'wp-rig-country-table' );

?>
<?php
		$countries = wp_rig()->get_countries();
		if ( ! empty( $countries ) ) :
			?>
<div class="country-table__wrapper">
<div class="country-table__content">
<tr>
        <th><input id="btn_copy" type="button" value="<?php _e('Copy Table');?>"></th>
</tr>
<table id="country-table">
    <tr>
        <th><?php _e('Country');?></th>
        <th><?php _e('Website');?></th>
        <th><?php _e('Email');?></th>
        <th><?php _e('Instagram');?></th>
        <th><?php _e('Twitter');?></th>
        <th><?php _e('Facebook');?></th>
        <th><?php _e('Youtube');?></th>
        <th><?php _e('Telegram');?></th>
        <th><?php _e('WhatsApp');?></th>
        <th><?php _e('LinkedIn');?></th>
        <th><?php _e('Donate');?></th>
        
    </tr>
        <?php
		// phpcs:disable WordPress.WP.GlobalVariablesOverride.Prohibited, WordPress.WP.I18n.MissingTranslatorsComment
		foreach ( $countries as $post ) :
		setup_postdata( $post );
		?>
            <tr>
                <td><b><?php the_title(); ?></b></td>
                <td><a target="_blank" href="<?php the_field('web'); ?>"><?php the_field('web'); ?></a></td>
                <td><a target="_blank" href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></td>
                <td><a target="_blank" href="<?php the_field('instagram'); ?>"><?php the_field('instagram'); ?></a></td>
                <td><a target="_blank" href="<?php the_field('twitter'); ?>"><?php the_field('twitter'); ?></a></td>
                <td><a target="_blank" href="<?php the_field('facebook'); ?>"><?php the_field('facebook'); ?></a></td>
                <td><a target="_blank" href="<?php the_field('youtube'); ?>"><?php the_field('youtube'); ?></a></td>
                <td><a target="_blank" href="<?php the_field('youtube'); ?>"><?php the_field('telegram'); ?></a></td>
                <td><a target="_blank" href="<?php the_field('youtube'); ?>"><?php the_field('whatsapp'); ?></a></td>
                <td><a target="_blank" href="<?php the_field('linkedin'); ?>"><?php the_field('linkedin'); ?></a></td>
                <td><a target="_blank" href="<?php the_field('donate'); ?>"><?php the_field('donate'); ?></a></td>
            </tr>
        
        <?php if( have_rows('other_organizations') ): ?>
        <?php while ( have_rows('other_organizations') ) : the_row(); ?>
            <tr>
                <td><b><?php the_title(); ?>:</b> <?php the_sub_field('organisation_name'); ?></th>
                <td><a target="_blank" href="<?php the_sub_field('web-opt'); ?>"><?php the_field('web-opt'); ?></a></td>
                <td><a target="_blank" href="mailto:<?php the_sub_field('email-opt'); ?>"><?php the_sub_field('email-opt'); ?></a></td>
                <td><a target="_blank" href="<?php the_sub_field('instagram-opt'); ?>"><?php the_sub_field('instagram-opt'); ?></a></td>
                <td><a target="_blank" href="<?php the_sub_field('twitter-opt'); ?>"><?php the_sub_field('twitter-opt'); ?></a></td>
                <td><a target="_blank" href="<?php the_sub_field('facebook-opt'); ?>"><?php the_sub_field('facebook-opt'); ?></a></td>
                <td><a target="_blank" href="<?php the_sub_field('youtube'); ?>"><?php the_sub_field('youtube'); ?></a></td>
                <td><a target="_blank" href="<?php the_sub_field('youtube'); ?>"><?php the_sub_field('telegram'); ?></a></td>
                <td><a target="_blank" href="<?php the_sub_field('youtube'); ?>"><?php the_sub_field('whatsapp'); ?></a></td>
                <td><a target="_blank" href="<?php the_sub_field('linkedin'); ?>"><?php the_sub_field('linkedin'); ?></a></td>
                <td><a target="_blank" href="<?php the_sub_field('donate'); ?>"><?php the_sub_field('donate'); ?></a></td>
            </tr>
        <?php endwhile; ?>
        <?php endif; ?>
        
   <?php 
   endforeach;
   wp_reset_postdata();
   ?>
   </table>
   <div id="row-count"></div>
   </div>
   </div>
<?php endif;