<?php
/**
 * Template part for displaying the header searchform
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

?>

<div class="city-search search-form-wrapper">
	<form role="search" aria-label="<?php esc_attr_e( 'City search', 'wp-rig' ); ?>" method="get" class="search-form expandable-search js-expandable-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<div class="input-control">
			<label>
				<span class="screen-reader-text"><?php echo esc_html( _x( 'Search for:', 'label' ) ); ?></span>
				<input type="search" class="search-field expandable-search__input js-expandable-search__input" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'wp-rig' ); ?>" value="<?php get_search_query(); ?>" name="s">
			</label>
			<button class="expandable-search__btn button has-theme-white-background-color has-theme-green-color" aria-label="<?php echo esc_attr_x( 'Toggle Search', 'wp-rig' ); ?>">
				<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M23.809 21.646l-6.205-6.205c1.167-1.605 1.857-3.579 1.857-5.711 0-5.365-4.365-9.73-9.731-9.73-5.365 0-9.73 4.365-9.73 9.73 0 5.366 4.365 9.73 9.73 9.73 2.034 0 3.923-.627 5.487-1.698l6.238 6.238 2.354-2.354zm-20.955-11.916c0-3.792 3.085-6.877 6.877-6.877s6.877 3.085 6.877 6.877-3.085 6.877-6.877 6.877c-3.793 0-6.877-3.085-6.877-6.877z"/></svg>
				<?php
				// uncomment this icon as soon as city search is ready
				/*<svg class="icon" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 200 200" xml:space="preserve"><path d="M140.1 149l-19.7-19.7c3.2-4.8 5-10.5 5-16.6 0-7.2-2.5-13.9-7-19.3 4-6.5 7-14.1 7-22 0-16.6-13.5-30.1-30.1-30.1S65.2 54.8 65.2 71.4c0 7.9 3 15.4 7 22-4.5 5.4-7 12.2-7 19.3 0 16.6 13.5 30.1 30.1 30.1 7.5 0 14.4-2.8 19.6-7.3l19.3 19.3 5.9-5.8zM95.3 49.5c12.1 0 21.9 9.8 21.9 21.9 0 15.7-15.7 30.7-21.9 36-6.2-5.3-21.9-20.2-21.9-36 0-12 9.8-21.9 21.9-21.9zm-21.9 63.2c0-4.4 1.3-8.6 3.7-12.2 7.2 9.1 15.1 15.1 15.7 15.5.7.5 1.6.8 2.5.8s1.7-.3 2.5-.8c.6-.5 8.5-6.4 15.7-15.5 2.4 3.6 3.7 7.8 3.7 12.2 0 5.4-2 10.3-5.2 14.1l-2.6 2.6c-3.8 3.2-8.7 5.2-14.1 5.2-12.1 0-21.9-9.8-21.9-21.9z"/><circle cx="95" cy="70.6" r="9.8"/></svg> */ ?>
			</button>
			<button type="submit" class="search-submit" aria-label="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>">
				<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M23.809 21.646l-6.205-6.205c1.167-1.605 1.857-3.579 1.857-5.711 0-5.365-4.365-9.73-9.731-9.73-5.365 0-9.73 4.365-9.73 9.73 0 5.366 4.365 9.73 9.73 9.73 2.034 0 3.923-.627 5.487-1.698l6.238 6.238 2.354-2.354zm-20.955-11.916c0-3.792 3.085-6.877 6.877-6.877s6.877 3.085 6.877 6.877-3.085 6.877-6.877 6.877c-3.793 0-6.877-3.085-6.877-6.877z"/></svg>
			</button>
		</div>
	</form>
</div><!-- .city-search -->
