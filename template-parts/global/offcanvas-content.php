<?php
/**
 * The template for displaying the offcanvas content
 *
 * Contains the main navigation
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

?>

<div id="main-offcanvas-right" class="offcanvas-right offcanvas__wrapper">

	<div class="offcanvas__content">

		<button class="button is-close offcanvas-close-btn">
			<span></span>
			<span></span>
			<span></span>
		</button>

		<nav class="offcanvas__nav" role="navigation" aria-label="Main">
			<?php wp_rig()->display_primary_nav_menu( [ 'menu_id' => 'offcanvas-menu' ] ); ?>
		</nav>

		<div class="offcanvas__footer">
			<?php get_template_part( 'template-parts/footer/info' ); ?>
		</div>

	</div>
</div>
