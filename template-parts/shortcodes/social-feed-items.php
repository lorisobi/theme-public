<?php
/**
 * The template for displaying the FFF social feed items
 *
 * Uses CPT data
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

$items = get_query_var( 'feed_items' );
if( empty( $items ) ) {
  return;
}

// phpcs:disable WordPress.WP.GlobalVariablesOverride.Prohibited, WordPress.WP.I18n.MissingTranslatorsComment
foreach ( $items as $post ) :
  setup_postdata( $post );
  $channel = isset( $post->fields['channel_data'] ) ? $post->fields['channel_data'] : null;
  ?>
  <li class="grid-item">
    <?php
    // header.
    if ( ! empty( $channel ) ) :
      ?>
      <header class="social-feed__header">
        <?php
        // channel logo.
        if ( ! empty( $channel['image_url'] ) ) :
          ?>
          <a class="social-feed__link social-feed__channel-logo" href="<?php echo esc_url( $channel['link'] ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Read on %s', 'wp-rig' ), $channel['name'] ) ); ?>">
            <img src="<?php echo esc_attr( $channel['image_url'] ); ?>">
          </a>
          <?php
        endif;
        ?>
        <?php
        // channel name.
        if ( ! empty( $channel['name'] ) ) :
          ?>
          <div class="social-feed__channel-title">
            <a class="social-feed__link social-feed__channel-name" href="<?php echo esc_url( $channel['link'] ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Read on %s', 'wp-rig' ), $channel['name'] ) ); ?>">
              <?php echo esc_html( $channel['name'] ); ?>
            </a>
            <span class="entry-meta social-feed__date"><?php echo get_the_date() . ' – ' . get_the_time(); ?></span>

            <?php
            // twitter meta.
            if ( ! empty( $channel['type'] ) && 'twitter' === $channel['type'] ) :
              ?>
              <div class="entry-meta social-feed__twitter-meta">
                <?php
                if ( ! empty( $channel['followers_count'] ) ) :
                  ?>
                  <span class="social-feed__followers"><span class="social-feed__label"><?php esc_html_e( 'Followers:', 'wp-rig' ); ?></span> <?php echo esc_html( $channel['followers_count'] ); ?></span>
                  <?php
                endif;
                ?>
              </div>
              <?php
            endif;
            ?>
          
          </div>
          <?php
        endif;
        ?>
      </header>
      <?php
    endif;
    ?>
<style>
.js-flickity{
  margin: 0 calc(var(--global-padding)*-1) var(--global-margin);
}
.gallery-cell {
  width:100%
}
.gallery-cell .social-feed__image-link{
  margin:unset;
}
</style>
    <?php
    // image. 2DO: add image slider/lightbox?
    if ( ! empty( $post->fields['images'] ) ) : 

      $post_images = get_field('images');
      $post_images_count = count($post_images);      

if( $post_images_count > 1 ): ?>


<?php if( have_rows('images') ): ?>

  <div class=" js-flickity" data-flickity-options='{ "cellAlign": "left" }'>
     <?php foreach ( $post->fields['images'] as $array ) :
        ?>
        <div class="gallery-cell">
        <a class="social-feed__link social-feed__image-link" href="<?php echo esc_url( $post->fields['link'] ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Read on %s', 'wp-rig' ), $channel['name'] ) ); ?>">
          <img class="social-feed__image" src="<?php echo esc_attr( $array['image'] ); ?>">
        </a>
        </div>
        <?php
      endforeach; ?>
      </div>

<?php endif; ?>
<!-- Close full image slider -->


<?php else : ?>
<!-- If only have one image do the below -->



<?php if( have_rows('images') ): ?>


        <?php while( have_rows('images') ): the_row(); 
            ?>
                <a class="social-feed__link social-feed__image-link" href="<?php echo esc_url( $post->fields['link'] ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Read on %s', 'wp-rig' ), $channel['name'] ) ); ?>">
          <img class="social-feed__image" src="<?php echo get_sub_field('image'); ?>">
        </a>

            <?php endwhile; ?>


<?php endif; ?>
<!-- Close full image slider -->

<?php endif; ?>


    
    <?php  
    endif;
    ?>

    <?php
    // description.
    if ( ! empty( $post->fields['description'] ) ) :
      ?>
      <a class="social-feed__link social-feed__description" href="<?php echo esc_url( $post->fields['link'] ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Read on %s', 'wp-rig' ), $channel['name'] ) ); ?>">
        <?php echo $post->fields['description']; ?>
      </a>
      <?php
    endif;
    ?>

    <?php /* the_title( '<h5 class="social-feed__title"><a class="social-feed__link" href="' . esc_url( $post->fields['link'] ) . '" target="_blank" rel="noopener" title="' . esc_attr( sprintf( __( 'Read on %s', 'wp-rig' ), $channel['name'] ) ) . '">', '</a></h5>' ); */ ?>

    <?php // footer. ?>
    <footer class="entry-meta social-feed__footer">
      <?php
      // likes, shares, comments count.
      if ( ! empty( $post->fields['likes_count'] ) ) :
        ?>
        <span class="social-feed__likes"><span class="social-feed__label"><?php esc_html_e( 'Likes:', 'wp-rig' ); ?></span> <?php echo esc_html( $post->fields['likes_count'] ); ?></span>
        <?php
      endif;
      if ( ! empty( $post->fields['shares_count'] ) ) :
        ?>
        <span class="social-feed__shares"><span class="social-feed__label"><?php esc_html_e( 'Shares:', 'wp-rig' ); ?></span> <?php echo esc_html( $post->fields['shares_count'] ); ?></span>
        <?php
      endif;
      if ( ! empty( $post->fields['comments_count'] ) ) :
        ?>
        <span class="social-feed__comments"><span class="social-feed__label"><?php esc_html_e( 'Comments:', 'wp-rig' ); ?></span> <?php echo esc_html( $post->fields['comments_count'] ); ?></span>
        <?php
      endif;
      ?>
    </footer>

  </li>
  <?php
  endforeach;
  wp_reset_postdata();
  // phpcs:enable
?>
