<?php
/**
 * The template for displaying the FFF social feed
 *
 * Uses CPT data
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

// Load custom styles.
wp_rig()->print_styles( 'wp-rig-social-feed' );

?>
<div class="social-feed__wrapper">

	<div class="social-feed__content">

		<?php
		$items = wp_rig()->get_social_feed_items();
		if ( ! empty( $items ) ) :
			?>

			<ul class="social-feed colcade-grid unitialized">
				<?php
					set_query_var( 'feed_items', $items );
					get_template_part( 'template-parts/shortcodes/social-feed', 'items' );
				?>
			</ul>

			<div class="wp-block-button aligncenter">
				<a class="social-feed__more wp-block-button__link has-background has-theme-blue-background-color" href="#"><?php esc_html_e( 'Load more', 'wp-rig' ); ?></a>
			</div>

		<?php else : ?>

			<p><?php esc_html_e( 'Sorry, our social feed currently happens to be empty!', 'wp-rig' ); ?></p>

			<?php
		endif;
		?>

	</div>
</div>
