<?php
/**
 * Template part for displaying countries social links
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;
// Load custom styles.
wp_rig()->print_styles( 'wp-rig-country-list' );
?>

<style>
.country-list__contact-icons{flex-wrap: wrap;}
.country-list__icon svg{height:2.5rem;}
@media screen and (max-width: 640px) {.country-list__icon svg{height:2rem;}}
.country-list__contact-icons a:not(:last-child){margin-right: var(--global-margin);}
</style>
<div class="entry-content has-text-align-center">
<h3><?php echo esc_attr( sprintf( __( 'Follow FFF %s', 'wp-rig' ), $post->post_title ) ); ?></h3>
<div class="country-list__contact-icons">
					<?php $countries = wp_rig()->get_countries();
					// email.
					if ( get_field('email') ) :
						$email_link_content = '<span class="screen-reader-text">' . esc_html( 'Email', 'wp-rig' ) . '</span>
								<span class="country-list__icon">
									<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="envelope" class="svg-inline--fa fa-envelope fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"></path></svg>
								</span>';
							?>
							<a class="country-list__email" href="mailto:<?php echo esc_attr( the_field('email') ); ?>" title="<?php echo esc_attr( sprintf( __( 'Email FridaysForFuture %s', 'wp-rig' ), $post->post_title ) ); ?>">
								<?php echo $email_link_content; ?>
							</a>
							<?php
						endif;
					?>

                    <?php
					// website.
					if ( get_field('web') ) :
						?>
							<a class="country-list__web" href="<?php echo esc_attr( the_field('web') ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Visit the website of FridaysForFuture %s', 'wp-rig' ), $post->post_title ) ); ?>">
								<span class="screen-reader-text"><?php esc_html_e( 'Website', 'wp-rig' ); ?></span>
								<span class="country-list__icon">
									<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="globe-americas" class="svg-inline--fa fa-globe-americas fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512"><path fill="currentColor" d="M248 8C111.03 8 0 119.03 0 256s111.03 248 248 248 248-111.03 248-248S384.97 8 248 8zm82.29 357.6c-3.9 3.88-7.99 7.95-11.31 11.28-2.99 3-5.1 6.7-6.17 10.71-1.51 5.66-2.73 11.38-4.77 16.87l-17.39 46.85c-13.76 3-28 4.69-42.65 4.69v-27.38c1.69-12.62-7.64-36.26-22.63-51.25-6-6-9.37-14.14-9.37-22.63v-32.01c0-11.64-6.27-22.34-16.46-27.97-14.37-7.95-34.81-19.06-48.81-26.11-11.48-5.78-22.1-13.14-31.65-21.75l-.8-.72a114.792 114.792 0 0 1-18.06-20.74c-9.38-13.77-24.66-36.42-34.59-51.14 20.47-45.5 57.36-82.04 103.2-101.89l24.01 12.01C203.48 89.74 216 82.01 216 70.11v-11.3c7.99-1.29 16.12-2.11 24.39-2.42l28.3 28.3c6.25 6.25 6.25 16.38 0 22.63L264 112l-10.34 10.34c-3.12 3.12-3.12 8.19 0 11.31l4.69 4.69c3.12 3.12 3.12 8.19 0 11.31l-8 8a8.008 8.008 0 0 1-5.66 2.34h-8.99c-2.08 0-4.08.81-5.58 2.27l-9.92 9.65a8.008 8.008 0 0 0-1.58 9.31l15.59 31.19c2.66 5.32-1.21 11.58-7.15 11.58h-5.64c-1.93 0-3.79-.7-5.24-1.96l-9.28-8.06a16.017 16.017 0 0 0-15.55-3.1l-31.17 10.39a11.95 11.95 0 0 0-8.17 11.34c0 4.53 2.56 8.66 6.61 10.69l11.08 5.54c9.41 4.71 19.79 7.16 30.31 7.16s22.59 27.29 32 32h66.75c8.49 0 16.62 3.37 22.63 9.37l13.69 13.69a30.503 30.503 0 0 1 8.93 21.57 46.536 46.536 0 0 1-13.72 32.98zM417 274.25c-5.79-1.45-10.84-5-14.15-9.97l-17.98-26.97a23.97 23.97 0 0 1 0-26.62l19.59-29.38c2.32-3.47 5.5-6.29 9.24-8.15l12.98-6.49C440.2 193.59 448 223.87 448 256c0 8.67-.74 17.16-1.82 25.54L417 274.25z"></path></svg>
								</span>
							</a>
						<?php
					endif;
					?>
                    <?php
					// instagram.
					if ( get_field('instagram') ) :
						?>
							<a class="country-list__instagram" href="<?php echo esc_attr( the_field('instagram') ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Visit %1$s\'s FridaysForFuture page on %2$s', 'wp-rig' ), $post->post_title, __( 'Instagram', 'wp-rig' ) ) ); ?>">
								<span class="screen-reader-text"><?php esc_html_e( 'Instagram', 'wp-rig' ); ?></span>
								<span class="country-list__icon">
								<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" class="svg-inline--fa fa-instagram fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><defs><radialGradient id="rg" r="150%" cx="30%" cy="107%"><stop stop-color="#fdf497" offset="0" /><stop stop-color="#fdf497" offset="0.05" /><stop stop-color="#fd5949" offset="0.45" /><stop stop-color="#d6249f" offset="0.6" /><stop stop-color="#285AEB" offset="0.9" /></radialGradient></defs><path fill="url('#rg')" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg>
								</span>
							</a>
						<?php
					endif;
					?>
					<?php
					// twitter.
					if ( get_field('twitter') ) :
						?>
							<a class="country-list__twitter" href="<?php echo esc_attr( the_field('twitter') ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Visit %1$s\'s FridaysForFuture page on %2$s', 'wp-rig' ), $post->post_title, __( 'Twitter', 'wp-rig' ) ) ); ?>">
								<span class="screen-reader-text"><?php esc_html_e( 'Twitter', 'wp-rig' ); ?></span>
								<span class="country-list__icon">
									<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" class="svg-inline--fa fa-twitter fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg>
								</span>
							</a>
						<?php
					endif;
					?>
					<?php
					// facebook.
					if ( get_field('facebook') ) :
						?>
							<a class="country-list__facebook" href="<?php echo esc_attr( the_field('facebook') ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Visit %1$s\'s FridaysForFuture page on %2$s', 'wp-rig' ), $post->post_title, __( 'Facebook', 'wp-rig' ) ) ); ?>">
								<span class="screen-reader-text"><?php esc_html_e( 'Facebook', 'wp-rig' ); ?></span>
								<span class="country-list__icon">
									<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" class="svg-inline--fa fa-facebook-f fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg>
								</span>
							</a>
						<?php
					endif;
					?>
					<?php
					// youtube.
					if ( get_field('youtube') ) :
						?>
							<a class="country-list__youtube" href="<?php echo esc_attr( the_field('youtube') ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Visit %1$s\'s FridaysForFuture page on %2$s', 'wp-rig' ), $post->post_title, __( 'Youtube', 'wp-rig' ) ) ); ?>">
								<span class="screen-reader-text"><?php esc_html_e( 'Youtube', 'wp-rig' ); ?></span>
								<span class="country-list__icon">
									<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="youtube" class="svg-inline--fa fa-youtube fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"></path></svg>
								</span>
							</a>
						<?php
					endif;
					?>
					<?php
					// linked-in.
					if ( get_field('linkedin') ) :
						?>
							<a class="country-list__linkedin" href="<?php echo esc_attr( the_field('linkedin') ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Visit %1$s\'s FridaysForFuture page on %2$s', 'wp-rig' ), $post->post_title, __( 'LinkedIn', 'wp-rig' ) ) ); ?>">
								<span class="screen-reader-text"><?php esc_html_e( 'LinkedIn', 'wp-rig' ); ?></span>
								<span class="country-list__icon">
									<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin-in" class="svg-inline--fa fa-linkedin-in fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path></svg>
								</span>
							</a>
						<?php
					endif;
					?>
					<?php
					// donate.
					if ( get_field('donate') ) :
						?>
							<a class="country-list__donate" href="<?php echo esc_attr( the_field('donate') ); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( sprintf( __( 'Donate to FridaysForFuture %1$s', 'wp-rig' ), $post->post_title, __( 'Donate', 'wp-rig' ) ) ); ?>">
								<span class="screen-reader-text"><?php esc_html_e( 'Donate', 'wp-rig' ); ?></span>
								<span class="country-list__icon">
									<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="dollar-sign" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 288 512" class="svg-inline--fa fa-dollar-sign fa-w-9 fa-2x"><path fill="currentColor" d="M209.2 233.4l-108-31.6C88.7 198.2 80 186.5 80 173.5c0-16.3 13.2-29.5 29.5-29.5h66.3c12.2 0 24.2 3.7 34.2 10.5 6.1 4.1 14.3 3.1 19.5-2l34.8-34c7.1-6.9 6.1-18.4-1.8-24.5C238 74.8 207.4 64.1 176 64V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48h-2.5C45.8 64-5.4 118.7.5 183.6c4.2 46.1 39.4 83.6 83.8 96.6l102.5 30c12.5 3.7 21.2 15.3 21.2 28.3 0 16.3-13.2 29.5-29.5 29.5h-66.3C100 368 88 364.3 78 357.5c-6.1-4.1-14.3-3.1-19.5 2l-34.8 34c-7.1 6.9-6.1 18.4 1.8 24.5 24.5 19.2 55.1 29.9 86.5 30v48c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-48.2c46.6-.9 90.3-28.6 105.7-72.7 21.5-61.6-14.6-124.8-72.5-141.7z" class=""></path></svg>								</span>
							</a>
						<?php
					endif;
					?>
					</div>
				</li>
				
			</ul>
			<?php if( have_rows('other_organizations') ): ?>
						<div class="panel">
							<space></space>
							<?php while ( have_rows('other_organizations') ) : the_row(); ?>
								<div class="country-other__content">
									<div class="country-inner__main">
									<div class="country-other__title"><?php echo esc_html(get_sub_field('organisation_name')); ?></div>				
											<div class="country-list__contact-icons">
											<?php
											// email.
											if ( get_sub_field('email-opt') ) :
												$email_link_content = '<span class="screen-reader-text">' . esc_html( 'Email', 'wp-rig' ) . '</span>
														<span class="country-list__icon">
															<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="envelope" class="svg-inline--fa fa-envelope fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z"></path></svg>
														</span>';
												if ( function_exists( 'enkode_mailto' ) ) :
													echo enkode_mailto( $post->fields['email'], $email_link_content, '', sprintf( __( 'Email FridaysForFuture %s', 'wp-rig' ), $post->post_title ) );
												else :
													?>
													<a class="country-list__email" href="mailto:<?php echo esc_attr(get_sub_field('email-opt')); ?>" title="<?php echo esc_attr( 'Send a email to&nbsp;'. get_sub_field('organisation_name')); ?>">
														<?php echo $email_link_content; ?>
													</a>
													<?php
												endif;
											endif;
											?>

											<?php
											// website.
											if ( get_sub_field('web-opt') ) :
												?>
													<a class="country-list__web" href="<?php echo esc_url(get_sub_field('web-opt')); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( "Visit&nbsp;". get_the_title() ."'s&nbsp;". get_sub_field('organisation_name') ."&nbsp;website!" ); ?>">
														<span class="screen-reader-text"><?php esc_html_e( 'Website', 'wp-rig' ); ?></span>
														<span class="country-list__icon">
															<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="globe-americas" class="svg-inline--fa fa-globe-americas fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512"><path fill="currentColor" d="M248 8C111.03 8 0 119.03 0 256s111.03 248 248 248 248-111.03 248-248S384.97 8 248 8zm82.29 357.6c-3.9 3.88-7.99 7.95-11.31 11.28-2.99 3-5.1 6.7-6.17 10.71-1.51 5.66-2.73 11.38-4.77 16.87l-17.39 46.85c-13.76 3-28 4.69-42.65 4.69v-27.38c1.69-12.62-7.64-36.26-22.63-51.25-6-6-9.37-14.14-9.37-22.63v-32.01c0-11.64-6.27-22.34-16.46-27.97-14.37-7.95-34.81-19.06-48.81-26.11-11.48-5.78-22.1-13.14-31.65-21.75l-.8-.72a114.792 114.792 0 0 1-18.06-20.74c-9.38-13.77-24.66-36.42-34.59-51.14 20.47-45.5 57.36-82.04 103.2-101.89l24.01 12.01C203.48 89.74 216 82.01 216 70.11v-11.3c7.99-1.29 16.12-2.11 24.39-2.42l28.3 28.3c6.25 6.25 6.25 16.38 0 22.63L264 112l-10.34 10.34c-3.12 3.12-3.12 8.19 0 11.31l4.69 4.69c3.12 3.12 3.12 8.19 0 11.31l-8 8a8.008 8.008 0 0 1-5.66 2.34h-8.99c-2.08 0-4.08.81-5.58 2.27l-9.92 9.65a8.008 8.008 0 0 0-1.58 9.31l15.59 31.19c2.66 5.32-1.21 11.58-7.15 11.58h-5.64c-1.93 0-3.79-.7-5.24-1.96l-9.28-8.06a16.017 16.017 0 0 0-15.55-3.1l-31.17 10.39a11.95 11.95 0 0 0-8.17 11.34c0 4.53 2.56 8.66 6.61 10.69l11.08 5.54c9.41 4.71 19.79 7.16 30.31 7.16s22.59 27.29 32 32h66.75c8.49 0 16.62 3.37 22.63 9.37l13.69 13.69a30.503 30.503 0 0 1 8.93 21.57 46.536 46.536 0 0 1-13.72 32.98zM417 274.25c-5.79-1.45-10.84-5-14.15-9.97l-17.98-26.97a23.97 23.97 0 0 1 0-26.62l19.59-29.38c2.32-3.47 5.5-6.29 9.24-8.15l12.98-6.49C440.2 193.59 448 223.87 448 256c0 8.67-.74 17.16-1.82 25.54L417 274.25z"></path></svg>
														</span>
													</a>
												<?php
											endif;
											?>
											<?php
											// instagram.
											if ( get_sub_field('instagram-opt') ) :
												?>
													<a class="country-list__instagram" href="<?php echo esc_url(get_sub_field('instagram-opt')); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( "Visit&nbsp;". get_the_title() ."'s&nbsp;". get_sub_field('organisation_name') ."&nbsp;Instagram page!" ); ?>">
														<span class="screen-reader-text"><?php esc_html_e( 'Instagram', 'wp-rig' ); ?></span>
														<span class="country-list__icon">
														<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" class="svg-inline--fa fa-instagram fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><defs><radialGradient id="rg" r="150%" cx="30%" cy="107%"><stop stop-color="#fdf497" offset="0" /><stop stop-color="#fdf497" offset="0.05" /><stop stop-color="#fd5949" offset="0.45" /><stop stop-color="#d6249f" offset="0.6" /><stop stop-color="#285AEB" offset="0.9" /></radialGradient></defs><path fill="url('#rg')" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg>
														</span>
													</a>
												<?php
											endif;
											?>
											<?php
											// twitter.
											if ( get_sub_field('twitter-opt') ) :
												?>
													<a class="country-list__twitter" href="<?php echo esc_url(get_sub_field('twitter-opt')); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( "Visit&nbsp;". get_the_title() ."'s&nbsp;". get_sub_field('organisation_name') ."&nbsp;Twitter page!"); ?>">
														<span class="screen-reader-text"><?php esc_html_e( 'Twitter', 'wp-rig' ); ?></span>
														<span class="country-list__icon">
															<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" class="svg-inline--fa fa-twitter fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg>
														</span>
													</a>
												<?php
											endif;
											?>
											<?php
											// facebook.
											if ( get_sub_field('facebook-opt') ) :
												?>
													<a class="country-list__facebook" href="<?php echo esc_url(get_sub_field('facebook-opt')); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( "Visit&nbsp;". get_the_title() ."'s&nbsp;". get_sub_field('organisation_name') ."&nbsp;Facebook page!"); ?>">
														<span class="screen-reader-text"><?php esc_html_e( 'Facebook', 'wp-rig' ); ?></span>
														<span class="country-list__icon">
															<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" class="svg-inline--fa fa-facebook-f fa-w-10" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg>
														</span>
													</a>
												<?php
											endif;
											?>
											<?php
											// youtube.
											if ( get_sub_field('youtube-opt') ) :
												?>
													<a class="country-list__youtube" href="<?php echo esc_url(get_sub_field('youtube-opt')); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( "Visit&nbsp;". get_the_title() ."'s&nbsp;". get_sub_field('organisation_name') ."&nbsp;YouTube channel!"); ?>">
														<span class="screen-reader-text"><?php esc_html_e( 'Youtube', 'wp-rig' ); ?></span>
														<span class="country-list__icon">
															<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="youtube" class="svg-inline--fa fa-youtube fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"></path></svg>
														</span>
													</a>
												<?php
											endif;
											?>
											<?php
											// telegram.
											if ( get_sub_field('telegram-opt') ) :
												?>
													<a class="country-list__telegram" href="<?php echo esc_url(get_sub_field('telegram-opt')); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( "Visit&nbsp;". get_the_title() ."'s&nbsp;". get_sub_field('organisation_name') ."&nbsp;Telegram group!"); ?>">
														<span class="screen-reader-text"><?php esc_html_e( 'Telegram', 'wp-rig' ); ?></span>
														<span class="country-list__icon">
											            <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="telegram" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" class="svg-inline--fa fa-telegram fa-w-16 fa-2x"><path fill="currentColor" d="M248 8C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zm121.8 169.9l-40.7 191.8c-3 13.6-11.1 16.9-22.4 10.5l-62-45.7-29.9 28.8c-3.3 3.3-6.1 6.1-12.5 6.1l4.4-63.1 114.9-103.8c5-4.4-1.1-6.9-7.7-2.5l-142 89.4-61.2-19.1c-13.3-4.2-13.6-13.3 2.8-19.7l239.1-92.2c11.1-4 20.8 2.7 17.2 19.5z" class=""></path></svg>			</span>
													</a>
												<?php
											endif;
											?>
											<?php
											// whatsapp.
											if ( get_sub_field('whatsapp-opt') ) :
												?>
													<a class="country-list__whatsapp" href="<?php echo esc_url(get_sub_field('whatsapp-opt')); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( "Visit&nbsp;". get_the_title() ."'s&nbsp;". get_sub_field('organisation_name') ."&nbsp;Whatsapp group!"); ?>">
														<span class="screen-reader-text"><?php esc_html_e( 'whatsapp', 'wp-rig' ); ?></span>
														<span class="country-list__icon">
                                                        <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="whatsapp" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-whatsapp fa-w-14 fa-2x"><path fill="currentColor" d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z" class=""></path></svg>													</a>
												<?php
											endif;
											?>
											<?php
											// linked-in.
											if ( get_sub_field('linkedin-opt') ) :
												?>
													<a class="country-list__linkedin" href="<?php echo esc_url(get_sub_field('linkedin-opt')); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( "Visit&nbsp;". get_the_title() ."'s&nbsp;". get_sub_field('organisation_name') ."&nbsp;LinkedIn page!"); ?>">
														<span class="screen-reader-text"><?php esc_html_e( 'LinkedIn', 'wp-rig' ); ?></span>
														<span class="country-list__icon">
															<svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin-in" class="svg-inline--fa fa-linkedin-in fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path></svg>
														</span>
													</a>
												<?php
											endif;
											?>
											<?php
											// donate.
											if ( get_sub_field('donate-opt') ) :
												?>
													<a class="country-list__donate" href="<?php echo esc_url(get_sub_field('donate-opt')); ?>" target="_blank" rel="noopener" title="<?php echo esc_attr( "Donate to&nbsp". get_the_title(). get_sub_field('organisation_name')); ?>">
														<span class="screen-reader-text"><?php esc_html_e( 'Donate', 'wp-rig' ); ?></span>
														<span class="country-list__icon">
															<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="dollar-sign" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 288 512" class="svg-inline--fa fa-dollar-sign fa-w-9 fa-2x"><path fill="currentColor" d="M209.2 233.4l-108-31.6C88.7 198.2 80 186.5 80 173.5c0-16.3 13.2-29.5 29.5-29.5h66.3c12.2 0 24.2 3.7 34.2 10.5 6.1 4.1 14.3 3.1 19.5-2l34.8-34c7.1-6.9 6.1-18.4-1.8-24.5C238 74.8 207.4 64.1 176 64V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48h-2.5C45.8 64-5.4 118.7.5 183.6c4.2 46.1 39.4 83.6 83.8 96.6l102.5 30c12.5 3.7 21.2 15.3 21.2 28.3 0 16.3-13.2 29.5-29.5 29.5h-66.3C100 368 88 364.3 78 357.5c-6.1-4.1-14.3-3.1-19.5 2l-34.8 34c-7.1 6.9-6.1 18.4 1.8 24.5 24.5 19.2 55.1 29.9 86.5 30v48c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-48.2c46.6-.9 90.3-28.6 105.7-72.7 21.5-61.6-14.6-124.8-72.5-141.7z" class=""></path></svg>								</span>
													</a>
												<?php
											endif;
											?>			
										</div>
									</div>
					<?php endwhile; ?>
        		<?php endif; ?>
				</li>
</div><!-- .entry-content -->
