// Accordion, credits to https://www.w3schools.com/


var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
	var elem = this
    if (elem.value=="▲ Show Less") elem.value = "▼ Show More";
    else elem.value = "▲ Show Less"; 
    this.classList.toggle("active");
    var panel = this.previousElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

	/* TOOLTIP Temporary */
jQuery(document).ready(function(){
  jQuery(".accordion").click(function(){
    jQuery(".tooltip_expand").addClass("tooltip_hide");
  });
});