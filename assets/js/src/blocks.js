/**
 * File blocks.js.
 *
 * adjust/enhance certain gutenberg blocks & respective settings
 */
// eslint-disable-next-line no-unused-vars
( function( $ ) {
	'use strict';

	// maybe add custom button styles
	/*wp.blocks.registerBlockStyle( 'core/button', {
		name: 'fff',
		label: 'FFF Button',
	});*/

	/*wp.blocks.registerBlockStyle( 'core/separator', {
		name: 'centered',
		label: 'Zentriert',
	} );*/

	// centered group block styles
	wp.blocks.registerBlockStyle( 'core/group', {
		name: 'reduced-width',
		label: 'Reduced width',
	} );
	wp.blocks.registerBlockStyle( 'core/group', {
		name: 'centered',
		label: 'Centered',
	} );

	// custom heading block styles
	wp.blocks.registerBlockStyle( 'core/heading', {
		name: 'underlined',
		label: 'Underlined',
	} );

	wp.blocks.registerBlockStyle( 'core/heading', {
		name: 'decorative',
		label: 'Decorative',
	} );

	// custom paragraph styles
	wp.blocks.registerBlockStyle( 'core/paragraph', {
		name: 'decorative',
		label: 'Decorative',
	} );
	wp.blocks.registerBlockStyle( 'core/paragraph', {
		name: 'highlight',
		label: 'Highlight',
	} );

	// unregister unneccessary block styles
	wp.domReady( function() {
		/*
		wp.blocks.unregisterBlockType( 'core/button' );
		wp.blocks.unregisterBlockType( 'core/verse' );
		wp.blocks.unregisterBlockType( 'core/preformatted' );
		wp.blocks.unregisterBlockType( 'core/latest-comments' );
		//console.log(wp.blocks.getBlockDefaultClassName);
		*/
	} );

	// adjust title block
	function setUnderlinedBlockHeading( element, type, props ) {
		// bail early, if no classes set
		if ( props.className === undefined ) {
			return element;
		}

		// if this a heading block?
		// eslint-disable-next-line no-undef
		if ( ! _.contains( [ 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' ], element.props.tagName ) ) {
			return element;
		}

		// check for suitable style
		if ( props.className.indexOf( 'is-style-underlined' ) === -1 ) {
			return element;
		}

		// no extra spans, plz
		if ( props.content.indexOf( '<span>' ) !== -1 ) {
			return element;
		}

		// finally add the span
		props.content = '<span>' + props.content + '</span>';

		return element;
	}

	wp.hooks.addFilter(
		'blocks.getSaveElement',
		'core/heading',
		setUnderlinedBlockHeading
	);
}( jQuery ) );
