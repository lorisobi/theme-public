// Copy complete table, credits to https://codepen.io/rishabhp

var copyBtn = document.querySelector('#btn_copy');
copyBtn.addEventListener('click', function () {
  var urlField = document.querySelector('#country-table');
   
  // create a Range object
  var range = document.createRange();  
  // set the Node to select the "range"
  range.selectNode(urlField);
  // add the Range to the set of window selections
  window.getSelection().addRange(range);
   
  // execute 'copy', can't 'cut' in this case
  document.execCommand('copy');
}, false);


// Outputs the number of rows in table

function CountRows() {
  var totalRowCount = 0;
  var rowCount = 0;
  var table = document.getElementById("country-table");
  var rows = table.getElementsByTagName("tr")
  for (var i = 0; i < rows.length; i++) {
      totalRowCount++;
      if (rows[i].getElementsByTagName("td").length > 0) {
          rowCount++;
      }
  }
  var message = "Total Countries and Country Organisations: " + rowCount;
  document.getElementById("row-count").innerHTML = message
}
window.onload = CountRows;