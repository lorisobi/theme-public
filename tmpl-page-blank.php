<?php
/**
 * Template Name: Blank
 * Template Post Type: post, page
 *
 * A blank template, without any margins or pre-added elements, only footer and header.
 * 
 * @link https://developer.wordpress.org/themes/template-files-section/page-template-files/#creating-custom-page-templates-for-global-use
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

get_header();

wp_rig()->print_styles( 'wp-rig-content' );
?>

<main id="primary" class="site-main">
    <article id="post-<?php the_ID(); ?>" <?php post_class( 'entry' ); ?>>
		<?php
		    while ( have_posts() ) {
		    	the_post();
		    	get_template_part( 'template-parts/content/entry_content', get_post_type() );
		    }
        ?>
    </article><!-- #post-<?php the_ID(); ?> -->
</main><!-- #primary -->

<?php
get_sidebar();
get_footer();