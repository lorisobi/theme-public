<?php
/**
 * Template Name: Full-width Keyvisual
 * Template Post Type: post, page
 *
 * Used for pages with full-width featured images
 *
 * @link https://developer.wordpress.org/themes/template-files-section/page-template-files/#creating-custom-page-templates-for-global-use
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

get_header();

wp_rig()->print_styles( 'wp-rig-content' );

?>
	<main id="primary" class="site-main">
		<?php

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/content/entry' );
		}
		?>
	</main><!-- #primary -->
<?php
get_sidebar();
get_footer();
